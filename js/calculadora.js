function sum(a, b) {
  let resulSuma = 0;

  resulSuma = a + b;
  return resulSuma;
}

function resta(a, b) {
  let resulResta = 0;
  resulResta = a - b;
  return resulResta;
}

function multiplica(a, b) {
  let resulMultiplica = 0;
  resulMultiplica = a * b;
  return resulMultiplica;
}

function division(a, b) {
  let resulDivide = 0;
  resulDivide = a / b;
  return resulDivide;
}

function reaizcuadrada(c) {
  let resulRaiz = 0;
  resulRaiz = Math.sqrt(c);
  return resulRaiz;
}

let a = prompt(
  "Escribe un valor para el primer número",
  "Introduzca un número"
);
let b = prompt(
  "Escribe un valor para el segundo número",
  "Introduzca un número"
);
a = Number(a);
b = Number(b);
if (a >= 0 && b >= 0) {
  alert(
    "el resultado de la operación es: Suma " +
      sum(a, b) +
      ", Resta " +
      resta(a, b) +
      ", Multiplicacion " +
      multiplica(a, b) +
      ", Division " +
      division(a, b)
  );
} else {
  if (a >= 0) {
    alert(
      "Como solo ha escrito un número le mostramos la raiz cuadrada de " +
        a +
        " es " +
        reaizcuadrada(a)
    );
  } else {
    if (b >= 0) {
      alert(
        "Como solo ha escrito un número le mostramos la raiz cuadrada de " +
          a +
          " es " +
          reaizcuadrada(b)
      );
    } else alert("Por favor introduzca números");
  }
}
